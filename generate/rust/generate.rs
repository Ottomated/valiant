
fn main() {
    make_ai(|memory, pheromone, in_front, has_food, is_east, is_stacked, random| {
        if in_front != InFront::Wall {
            Output { memory, pheromone, action: Action::MoveForward }
        } else if random {
            Output { memory, pheromone, action: Action::TurnRight }
        } else {
            Output { memory, pheromone, action: Action::TurnLeft }
        }
    });
    }

    fn encode_int(slice: &mut [bool], value: u16) {
        for i in 0..slice.len() {
            slice[i] = (value & (1 << i)) != 0;
        }
    }

    fn decode_int(slice: &[bool]) -> u16 {
        let mut result = 0;
        for i in 0..slice.len() {
            if slice[i] {
                result |= 1 << i;
            }
        }
        result
    }

    const MEMORY_BITS: usize = 8;
    const PHEROMONE_BITS: usize = 6;

    #[derive(PartialEq, Clone, Copy)]
    enum InFront { Empty, Wall, Food, FriendlyAnthill, EnemyAnthill, Friend, Enemy }

    enum Action { StayStill, TurnLeft, TurnRight, MoveForward }

    struct Output {
        memory: [bool; MEMORY_BITS],
        pheromone: [bool; PHEROMONE_BITS],
        action: Action,
    }

    fn make_ai(ai: impl Fn([bool; MEMORY_BITS], [bool; PHEROMONE_BITS], InFront, bool, bool, bool, bool) -> Output) {
        let mut rules = vec![0_u16; 1 << 21];

        for memory_bits in 0..1 << MEMORY_BITS {
            for pheromone_bits in 0..1 << PHEROMONE_BITS {
                for &has_food in &[false, true] {
                    for &is_east in &[false, true] {
                        for &is_stacked in &[false, true] {
                            for &random in &[false, true] {
                                for &in_front in &[InFront::Empty, InFront::Wall, InFront::Food, InFront::FriendlyAnthill, InFront::EnemyAnthill, InFront::Friend, InFront::Enemy] {
                                    let mut memory = [false; MEMORY_BITS];
                                    let mut pheromone = [false; PHEROMONE_BITS];
                                    for i in 0..MEMORY_BITS {
                                        memory[i] = (memory_bits & (1 << i)) != 0;
                                    }
                                    for i in 0..PHEROMONE_BITS {
                                        pheromone[i] = (pheromone_bits & (1 << i)) != 0;
                                    }
                                    let output = ai(memory, pheromone, in_front, has_food, is_east, is_stacked, random);

                                    let mut index = 0;
                                    if random {
                                        index |= 1;
                                    }
                                    index <<= 1;
                                    if is_stacked {
                                        index |= 1;
                                    }
                                    index <<= 1;
                                    if is_east {
                                        index |= 1;
                                    }
                                    index <<= 1;
                                    if has_food {
                                        index |= 1;
                                    }
                                    index <<= 3;
                                    index |= match in_front {
                                        InFront::Empty => 0,
                                        InFront::Wall => 1,
                                        InFront::Food => 2,
                                        InFront::FriendlyAnthill => 3,
                                        InFront::EnemyAnthill => 4,
                                        InFront::Friend => 5,
                                        InFront::Enemy => 6,
                                    };
                                    index <<= PHEROMONE_BITS;
                                    index |= pheromone_bits;
                                    index <<= MEMORY_BITS;
                                    index |= memory_bits;

                                    assert_eq!(0, rules[index]);

                                    let mut result = match output.action {
                                        Action::StayStill => 0,
                                        Action::TurnLeft => 1,
                                        Action::TurnRight => 2,
                                        Action::MoveForward => 3,
                                    };
                                    result <<= PHEROMONE_BITS;
                                    for i in 0..PHEROMONE_BITS {
                                        if output.pheromone[i] {
                                            result |= 1 << i;
                                        }
                                    }
                                    result <<= MEMORY_BITS;
                                    for i in 0..MEMORY_BITS {
                                        if output.memory[i] {
                                            result |= 1 << i;
                                        }
                                    }
                                    rules[index] = result;
                                }
                            }
                        }
                    }
                }
            }
        }
        for rule in rules {
            println!("{}", rule);
        }
    }

