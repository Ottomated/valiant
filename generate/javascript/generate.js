const fs = require('fs');

class Valiant {
	/**
	 * The Valiant compiler
	 * @constructor
	 * @param {number} [MEMORY_BITS=8] - How many bits of memory the AI has 
	 * @param {number} [PHEROMONE_BITS=6] - How many bits of pheromones tiles can store 
	 */
	constructor(MEMORY_BITS = 8, PHEROMONE_BITS = 6) {
		this.VERSION = '1.0.0';
		this.MEMORY_BITS = MEMORY_BITS;
		this.PHEROMONE_BITS = PHEROMONE_BITS;
		/**
		 * Possible actions for ants
		 * @readonly
		 * @enum {number}
		 */
		this.Action = {
			/** Don't move */
			STAY_STILL: 0,
			/** Turn 90 degrees to the left */
			TURN_LEFT: 1,
			/** Turn 90 degrees to the right */
			TURN_RIGHT: 2,
			/** Move forwards one tile */
			MOVE_FORWARD: 3
		};
		/**
		 * Compiles an AI function
		 * @param {function} ai - The function describing the AI
		 * @param {string=} fileName - File to write to: returns text if not set 
		 * @returns {string|undefined} - The compiled AI if no filename was passed
		 */
		this.compile = (ai, fileName = null) => {
			let rules = Array(1 << 21).fill(0);
			for (let memoryBits = 0; memoryBits < 1 << this.MEMORY_BITS; memoryBits++) {
				for (let pheromoneBits = 0; pheromoneBits < 1 << this.PHEROMONE_BITS; pheromoneBits++) {
					for (let hasFood = 0; hasFood < 2; hasFood++) {
						for (let isEast = 0; isEast < 2; isEast++) {
							for (let isStacked = 0; isStacked < 2; isStacked++) {
								for (let random = 0; random < 2; random++) {
									for (let inFront = 0; inFront < Object.values(this.InFront).length; inFront++) {
										let memory = Array(this.MEMORY_BITS).fill(false);
										for (let i = 0; i < this.MEMORY_BITS; i++) {
											memory[i] = (memoryBits & (1 << i)) != 0;
										}
										let pheromone = Array(this.PHEROMONE_BITS).fill(false);
										for (let i = 0; i < this.PHEROMONE_BITS; i++) {
											pheromone[i] = (pheromoneBits & (1 << i)) != 0;
										}

										let output = ai(memory, pheromone, inFront, hasFood, isEast, isStacked, random)
										let index = 0;
										if (random) {
											index |= 1;
										}
										index <<= 1;
										if (isStacked) {
											index |= 1;
										}
										index <<= 1;
										if (isEast) {
											index |= 1;
										}
										index <<= 1;
										if (hasFood) {
											index |= 1;
										}
										index <<= 3;
										index |= inFront
										index <<= this.PHEROMONE_BITS;
										index |= pheromoneBits;
										index <<= this.MEMORY_BITS;
										index |= memoryBits;

										if (rules[index] != 0) {
											console.error("WTF " + index + " " + random + " " + isStacked + " " + isEast + "  " + hasFood + " " + inFront + " " + pheromoneBits + " " + memoryBits);
										}
										let result = 0;

										result |= output.action;
										result <<= this.PHEROMONE_BITS;
										for (let i = 0; i < this.PHEROMONE_BITS; i++) {
											if (output.pheromone[i]) result |= 1 << i;
										}
										result 	<<= this.MEMORY_BITS;
										for (let i = 0; i < this.MEMORY_BITS; i++) {
											if (output.memory[i]) result |= 1 << i;
										}
										rules[index] = result;
									}
								}
							}
						}
					}
				}
			}
			let compiled = rules.join('\n') + '\n';
			console.log(compiled.length)
			if (fileName) {
				return fs.writeFileSync(fileName, compiled)
			} else {
				return compiled;
			}
		}
		/**
		 * Helper function for decoding integers from memory arrays
		 * @param {boolean[]} array - The array to decode from
		 * @param {number} offset - The zero-indexed position to start reading from
		 * @param {number} length - How many bytes to read from the array
		 * @returns {number} - The decoded number from those bytes
		 */
		this.decodeInt = (array, offset, length) => {
			let result = 0;
			for (let i = 0; i < length; i++) {
				if (array[offset + i]) {
					result |= 1 << i;
				}
			}
			return result;
		}
		/**
		 * Helper function for encoding integers to memory arrays
		 * @param {boolean[]} array - The array to encode to
		 * @param {number} offset - The zero-indexed position to start writing to
		 * @param {number} length - How many bytes to write to the array
		 * @param {number} value - The integer to write to the array
		 * @returns {undefined}
		 */
		this.encodeInt = (array, offset, length, value) => {
			for (let i = 0; i < length; i++) {
				array[offset + i] = (value & (1 << i)) != 0;
			}
		}
		/**
		 * Enum for what the ant can see in front of it
		 * @readonly
		 * @enum {number}
		 */
		this.InFront = {
			/** Nothing */
			EMPTY: 0,
			/** Wall */
			WALL: 1,
			/** Food source */
			FOOD: 2,
			/** Friendly Anthill */
			FRIENDLY_ANTHILL: 3,
			/** Enemy Anthill */
			ENEMY_ANTHILL: 4,
			/** Friendly Ant */
			FRIEND: 5,
			/** Enemy Ant */
			ENEMY: 6
		};

		class Output {
			/**
			 * The output object returned by the AI function
			 * @param {boolean[]} memory - The memory to write
			 * @param {boolean[]} pheromone - The pheromone to write
			 * @param {Action} action - The action to take
			 */
			constructor(memory, pheromone, action) {
				if (memory.length !== MEMORY_BITS) throw "Illegal memory length";
				if (pheromone.length !== PHEROMONE_BITS) throw "Illegal pheromone length";
				this.memory = memory;
				this.pheromone = pheromone;
				this.action = action;
			}
		};
		this.Output = Output;

	}
}
module.exports = Valiant;
