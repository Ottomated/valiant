const Valiant = require('./generate.js'),
    valiant = new Valiant();

let AI = (memory, pheromone, inFront, hasFood, isEast, isStacked, random) => {

    // If not initialized
    if (!memory[0]) {
        // Turn right until east
        if (!isEast) {
            return new valiant.Output(memory, pheromone, valiant.Action.TURN_RIGHT);
        }
        // Initialize
        memory[0] = true;
        // Encode the direction:
        // 0: NORTH, 1: EAST, 2: SOUTH, 3: WEST
        valiant.encodeInt(memory, 1, 2, 1);
    }
    // Get current direction
    let direction = valiant.decodeInt(memory, 1, 2);

    // If pheromone not set, set it to the opposite of our direction
    if (!pheromone[0]) {
        pheromone[0] = true;
        valiant.encodeInt(pheromone, 1, 2, direction + 2);
    }

    // If we have food, go home, otherwise, wander randomly
    let action;
    if (hasFood) {
        // If we are facing the direction the pheromone tells us, go straight
        if (valiant.decodeInt(memory, 1, 2) == valiant.decodeInt(pheromone, 1, 2)) {
            action = valiant.Action.MOVE_FORWARD;
            // Otherwise, turn left
        } else {
            valiant.encodeInt(memory, 1, 2, direction + 1);
            action = valiant.Action.TURN_LEFT;
        }
        // Randomly go forward
    } else if (random) {
        action = valiant.Action.MOVE_FORWARD;
        // Or right
    } else {
        // Store the new direction
        valiant.encodeInt(memory, 1, 2, direction + 1);
        action = valiant.Action.TURN_RIGHT;
    }
    return new valiant.Output(memory, pheromone, action);
}
// Save to file
valiant.compile(exampleAI, 'JSAI.ant');
