import sys

template = '''
<html>

  <body>
    <div>
      Ticks Per Second: <input id="tps" type="text" value="1"></input>
      <br>
    </div>
    <canvas id="canvas" height=800 width=800></canvas>
  </body>

  <script>
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext("2d");
  
    let tps = document.getElementById("tps");
    let timeout = () => tps.value? 1000 / tps.value : 100;
    let t = 0;

    let game_data = {}; 

    let render = () => {
      ctx.clearRect(0, 0, canvas.width, canvas.height);

      let frame = game_data.frames[t];

      for(let x = 0; x < frame.length; x++) {
        for(let y = 0; y < frame[x].length; y++) {
          let tile_size = canvas.width / frame.length;

          let space = frame[x][y];
    
          let is_wall = space === 0;
          let is_anthill = space %% 4 === 1;
          let hill_owner = food_count = (space >> 2) %% 8;
          let ants_owner = (space >> 5) %% 8;
          let north_ant = (space & (1 << 8)) != 0;
          let east_ant = (space & (1 << 9)) != 0;
          let south_ant = (space & (1 << 10)) != 0;
          let west_ant = (space & (1 << 11)) != 0;
          let north_food = (space & (1 << 12)) != 0;
          let east_food = (space & (1 << 13)) != 0;
          let south_food = (space & (1 << 14)) != 0;
          let west_food = (space & (1 << 15)) != 0;

          if(is_anthill && north_ant) {
            console.log(x + " " + y + " " + space + " " + ants_owner);  
          }
      
          ctx.globalAlpha = 1;

          let player_color = owner => ["blue", "red", "orange", "cyan", "pink", "grey"][owner];

          if(is_wall) {
            ctx.fillStyle = "black";
            ctx.fillRect(x * tile_size, y * tile_size, tile_size, tile_size);
          } else if(is_anthill) {
              ctx.fillStyle = ctx.strokeStyle = player_color(hill_owner);
              ctx.beginPath();
              ctx.arc((x+0.5)*tile_size, (y+0.5)*tile_size, tile_size/4, 0, 2*Math.PI);
              ctx.stroke();
              ctx.fill();
              ctx.closePath();
          }

          ctx.fillStyle = ctx.strokeStyle = player_color(ants_owner);

          if(north_ant) {
            ctx.beginPath();
            ctx.moveTo((x+0.5)*tile_size, (y+1)*tile_size);
            ctx.lineTo((x+0.75)*tile_size, (y+0.75)*tile_size);
            ctx.lineTo((x+0.25)*tile_size, (y+0.75)*tile_size);
            ctx.closePath();
            if(north_food) {
              ctx.fill();
            } else {
              ctx.stroke();
            }
          }
          if(east_ant) {
            ctx.beginPath();
            ctx.moveTo((x+1)*tile_size, (y+0.5)*tile_size);
            ctx.lineTo((x+0.75)*tile_size, (y+0.75)*tile_size);
            ctx.lineTo((x+0.75)*tile_size, (y+0.25)*tile_size);
            ctx.closePath();
            if(east_food) {
              ctx.fill();
            } else {
              ctx.stroke();
            }
          }
          if(south_ant) {
            ctx.beginPath();
            ctx.moveTo((x+0.5)*tile_size, (y+0)*tile_size);
            ctx.lineTo((x+0.75)*tile_size, (y+0.25)*tile_size);
            ctx.lineTo((x+0.25)*tile_size, (y+0.25)*tile_size);
            ctx.closePath();
            if(south_food) {
              ctx.fill();
            } else {
              ctx.stroke();
            }
          }
          if(west_ant) {
            ctx.beginPath();
            ctx.moveTo((x+0)*tile_size, (y+0.5)*tile_size);
            ctx.lineTo((x+0.25)*tile_size, (y+0.75)*tile_size);
            ctx.lineTo((x+0.25)*tile_size, (y+0.25)*tile_size);
            ctx.closePath();
            if(west_food) {
              ctx.fill();
            } else {
              ctx.stroke();
            }
          }

          if(!is_wall && !is_anthill && food_count > 0) {
            ctx.fillStyle = ctx.strokeStyle = "green";
            ctx.globalAlpha = 1-1/(1+food_count);
            ctx.beginPath();
            ctx.arc((x+0.5)*tile_size, (y+0.5)*tile_size, tile_size/4, 0, 2*Math.PI);
            ctx.stroke();
            ctx.fill();
            ctx.closePath();
          }
        }
      }
      if(t + 1 != game_data.frames.length) {
        t += 1;
      }
      setTimeout(() => render(), timeout());
    };


    game_data = %s;

  render();
  </script>
</html>
'''

[_, i, o] = sys.argv

output = open(o, 'w')

output.write(template % open(i).read())
